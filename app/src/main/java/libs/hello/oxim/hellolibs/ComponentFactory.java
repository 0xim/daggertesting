package libs.hello.oxim.hellolibs;

import libs.hello.oxim.hellolibs.craps.ApplicationComponent;
import libs.hello.oxim.hellolibs.craps.CrapsComponent;

public final class ComponentFactory {

    public static ApplicationComponent createApplicationComponent(){
        return ApplicationComponent.Initializer.init();
    }

    public static CrapsComponent createCrapsComponent(CrapsApplication crapsApplication){
        //return CrapsComponent.Initializer.init(crapsApplication.getApplicationComponent());
        return null;
    }
}
