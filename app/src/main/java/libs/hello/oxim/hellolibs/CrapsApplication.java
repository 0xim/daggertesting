package libs.hello.oxim.hellolibs;

import android.app.Application;

import libs.hello.oxim.hellolibs.craps.ApplicationComponent;
import libs.hello.oxim.hellolibs.craps.CrapsComponent;

public class CrapsApplication extends Application {

    private ApplicationComponent applicationComponent;
    private CrapsComponent crapsComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.applicationComponent = ComponentFactory.createApplicationComponent();
    }

    public ApplicationComponent getApplicationComponent(){
        return this.applicationComponent;
    }

    public CrapsComponent createCrapsComponent(){
        this.crapsComponent = ComponentFactory.createCrapsComponent(this);
        return crapsComponent;
    }

    public void releaseCrapsComponent(){
        this.crapsComponent = null;
    }

}
