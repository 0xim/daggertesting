package libs.hello.oxim.hellolibs.craps;

import dagger.Component;
import libs.hello.oxim.hellolibs.ui.activity.CrapsActivity;
import libs.hello.oxim.hellolibs.ui.fragment.CrapsFragment;

@ActivityScope
@Component(
        dependencies = {
                ApplicationComponent.class
        },
        modules = {
                ActivityModule.class
        }
)
public interface ActivityComponent {

    void inject(CrapsFragment crapsFragment);

    final class Initializer {

        public static ActivityComponent init(ApplicationComponent applicationComponent){
            return DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule())
                    .applicationComponent(applicationComponent)
                    .build();
        }

        // prevents instances
        private Initializer(){

        }
    }

}
