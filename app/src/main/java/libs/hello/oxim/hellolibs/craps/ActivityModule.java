package libs.hello.oxim.hellolibs.craps;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import libs.hello.oxim.hellolibs.presenter.CrapsPresenter;
import libs.hello.oxim.hellolibs.presenter.CrapsPresenterImpl;

@Module
public final class ActivityModule {

    @Provides
    @ActivityScope
    Craps provideCraps(CrapsImpl crapsImpl){
        return crapsImpl;
    }

    @Provides
    @ActivityScope
    CrapsPresenter provideCrapsPresenter(CrapsPresenterImpl crapsPresenter){
        return crapsPresenter;
    }

    @Provides
    @ActivityScope
    Gson provideGson(){
        return new Gson();
    }
}
