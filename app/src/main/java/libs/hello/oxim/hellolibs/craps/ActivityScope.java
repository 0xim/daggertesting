package libs.hello.oxim.hellolibs.craps;

import javax.inject.Scope;

/**
 * Created by oxim on 07.07.15..
 */
@Scope
public @interface ActivityScope {
}
