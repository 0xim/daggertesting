package libs.hello.oxim.hellolibs.craps;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class
        }
)
public interface ApplicationComponent {

    Dice provideDice();

    final class Initializer {

        // Used to provide DaggerApplicationComponent that injects
        public static ApplicationComponent init() {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule())
                    .build();
        }

        // prevents instances
        private Initializer() {
        }

    }
}
