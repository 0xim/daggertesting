package libs.hello.oxim.hellolibs.craps;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import libs.hello.oxim.hellolibs.presenter.CrapsPresenter;
import libs.hello.oxim.hellolibs.presenter.CrapsPresenterImpl;

@Module
public final class ApplicationModule {

    @Provides
    @Singleton
    Dice provideDice(DiceImpl diceImpl) {
        return diceImpl;
    }

}
