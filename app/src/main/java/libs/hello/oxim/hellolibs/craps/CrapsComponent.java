package libs.hello.oxim.hellolibs.craps;

import dagger.Component;
import dagger.Subcomponent;
import libs.hello.oxim.hellolibs.ui.activity.CrapsActivity;
import libs.hello.oxim.hellolibs.ui.fragment.CrapsFragment;

@CrapsScope
@Subcomponent(
        modules = {
                CrapsModule.class
        }
)
public interface CrapsComponent {

    void inject(CrapsActivity crapsActivity);
    //void inject(CrapsFragment crapsFragment);

    final class Initializer {

        /*public static CrapsComponent init(ApplicationComponent applicationComponent){
            return DaggerCrapsComponent.builder()
                    .applicationComponent(applicationComponent)
                    .crapsModule(new CrapsModule())
                    .build();
        }*/

        private Initializer() {
        }

    }

}
