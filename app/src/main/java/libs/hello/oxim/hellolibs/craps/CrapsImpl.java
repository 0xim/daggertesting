package libs.hello.oxim.hellolibs.craps;

import android.util.Log;

import javax.inject.Inject;

/**
 * Created by oxim on 07.07.15..
 */
public final class CrapsImpl implements Craps {

    private final String TAG = this.getClass().getSimpleName();

    private final Dice dice;

    @Inject
    public CrapsImpl(Dice dice){
        this.dice = dice;
    }

    @Override
    public String throwDices() {
        return "Dices rolled " + dice.returnRandomNumber() + " and " + dice.returnRandomNumber();
    }
}
