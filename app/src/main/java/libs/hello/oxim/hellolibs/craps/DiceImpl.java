package libs.hello.oxim.hellolibs.craps;

import java.util.Random;

import javax.inject.Inject;
public final class DiceImpl implements Dice {

    private final String TAG = this.getClass().getSimpleName();

    Random random;

    @Inject
    public DiceImpl(){
        this.random = new Random();
    }

    @Override
    public int returnRandomNumber() {
        return random.nextInt(6)+1;
    }

}
