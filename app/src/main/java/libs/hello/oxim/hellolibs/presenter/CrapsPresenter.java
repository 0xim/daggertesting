package libs.hello.oxim.hellolibs.presenter;

/**
 * Created by Mihael on 07.07.15..
 */
public interface CrapsPresenter {

    void incrementCounter();

    int getCounterValue();

    void saveLastOutcome(String outcome);

    String getLastOutcome();

}
