package libs.hello.oxim.hellolibs.presenter;

import javax.inject.Inject;

public final class CrapsPresenterImpl implements CrapsPresenter {

    int counter = 0;
    String lastOutcome = "";

    @Inject
    public CrapsPresenterImpl(){
    }

    @Override
    public void incrementCounter() {
        counter++;
    }

    @Override
    public int getCounterValue() {
        return counter;
    }

    @Override
    public void saveLastOutcome(String outcome) {
        lastOutcome = outcome;
    }

    @Override
    public String getLastOutcome() {
        return lastOutcome;
    }
}
