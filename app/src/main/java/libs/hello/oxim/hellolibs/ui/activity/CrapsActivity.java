package libs.hello.oxim.hellolibs.ui.activity;

import android.app.Activity;

import android.os.Bundle;

import butterknife.ButterKnife;
import libs.hello.oxim.hellolibs.CrapsApplication;
import libs.hello.oxim.hellolibs.R;

public class CrapsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //((CrapsApplication) getApplication()).createCrapsComponent().inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
