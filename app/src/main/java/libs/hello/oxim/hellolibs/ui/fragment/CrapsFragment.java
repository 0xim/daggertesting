package libs.hello.oxim.hellolibs.ui.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import libs.hello.oxim.hellolibs.CrapsApplication;
import libs.hello.oxim.hellolibs.R;
import libs.hello.oxim.hellolibs.craps.ActivityComponent;
import libs.hello.oxim.hellolibs.craps.Craps;
import libs.hello.oxim.hellolibs.craps.CrapsComponent;
import libs.hello.oxim.hellolibs.presenter.CrapsPresenter;

public class CrapsFragment extends Fragment {

    @Inject
    Craps craps;

    @Inject
    CrapsPresenter presenter;

    @Bind(R.id.txtLabel)
    protected TextView labelText;

    @Bind(R.id.txtNumberOfRolls)
    protected TextView numberOfRollsText;

    @Bind(R.id.btnRoll)
    protected Button rollButton;

    public CrapsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_craps, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //CrapsComponent.Initializer.init(((CrapsApplication) getActivity().getApplication()).getApplicationComponent()).inject(this);
        ActivityComponent.Initializer.init(((CrapsApplication) getActivity().getApplication()).getApplicationComponent()).inject(this);
    }

    @OnClick(R.id.btnRoll)
    public void rollDices(){
        String outcome = this.craps.throwDices();
        updatePresenter(outcome);
        updateViews();
    }

    private void updateViews() {
        numberOfRollsText.setText("" + presenter.getCounterValue());
        labelText.setText(presenter.getLastOutcome());
    }

    private void updatePresenter(String outcome) {
        presenter.incrementCounter();
        presenter.saveLastOutcome(outcome);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
